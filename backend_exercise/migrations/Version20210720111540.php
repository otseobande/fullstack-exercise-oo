<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210720111540 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Create product table';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('
            CREATE TABLE product (
                id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                name VARCHAR(255) NOT NULL,
                sku VARCHAR(30) NOT NULL
            )
        ');

        $this->addSql('
            INSERT INTO product (`name`, `sku`) VALUES
            ("Classic T-Shirt", "N03"),
            ("Organic Light T-Shirt", "EP01"),
            ("Unisex Standard Softstyle T-Shirt", "GD001")
        ');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE product');
    }
}
