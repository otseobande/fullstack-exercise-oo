# EVERPRESS - Skills Assessment

Role: Full Stack Developer

**Note: Information communicated is strictly confidential and can't be shared outside the scope of the exercise.**

The full stack developer interview will focus on things that the team work on every day across the stack:

- Exposing the APIs that the frontend will consume and displaying this information for users
- Integrating with many external services and printing companies for production
- Pricing, printing and other important business logic
- Building new features and improving the overall experience for users

As part of the interview please complete this skills assessment.

The assessment comprises of two exercises: 

1. Backend Exercise - Creating an order API
2. Frontend Exercise - Consuming order APIs to display information on a UI